#include "command_client.h"
#include "command_common.h"

#include <stddef.h>
#include <stdint.h>

#include "access.h"
#include "access_config.h"
#include "access_reliable.h"
#include "device_state_manager.h"
#include "log.h"
#include "mesh_app_utils.h"
#include "nrf_mesh.h"
#include "nrf_mesh_assert.h"

/*****************************************************************************
 * Static variables
 *****************************************************************************/

/** Keeps a single global TID for all transfers. */
static uint8_t      m_tid;
static dsm_handle_t m_publish_address_handle;

/*****************************************************************************
 * Static functions
 *****************************************************************************/

static void reliable_status_cb(access_model_handle_t model_handle, void* p_args, access_reliable_status_t status)
{
    command_client_t* p_client = p_args;
    // NRF_MESH_ASSERT(p_client->status_cb != NULL);

    p_client->state.reliable_transfer_active = false;
    switch (status) {
        case ACCESS_RELIABLE_TRANSFER_SUCCESS:
            /* Ignore */
            break;
        case ACCESS_RELIABLE_TRANSFER_TIMEOUT:
            // p_client->status_cb(p_client, COMMAND_STATUS_ERROR_NO_REPLY, NRF_MESH_ADDR_UNASSIGNED);
            break;
        case ACCESS_RELIABLE_TRANSFER_CANCELLED:
            // p_client->status_cb(p_client, COMMAND_STATUS_CANCELLED, NRF_MESH_ADDR_UNASSIGNED);
            break;
        default:
            /* Should not be possible. */
            NRF_MESH_ASSERT(false);
            break;
    }
}

static uint32_t send_reliable_message(const command_client_t* p_client, command_opcode_t opcode, const uint8_t* p_data, uint16_t length)
{
    command_opcode_t reply_opcode = COMMAND_OPCODE_REGISTER;

    switch (opcode) {
        case COMMAND_OPCODE_FIND:
            reply_opcode = COMMAND_OPCODE_REGISTER;
            break;
        case COMMAND_OPCODE_SET:
            reply_opcode = COMMAND_OPCODE_STATUS;
            break;
        case COMMAND_OPCODE_GET:
            reply_opcode = COMMAND_OPCODE_DATA;
            break;
        default:
            NRF_MESH_ASSERT(false);
            break;
    }

    access_reliable_t reliable;
    reliable.model_handle              = p_client->model_handle;
    reliable.message.p_buffer          = p_data;
    reliable.message.length            = length;
    reliable.message.opcode.opcode     = opcode;
    reliable.message.opcode.company_id = COMMAND_COMPANY_ID;
    reliable.message.force_segmented   = false;
    reliable.message.transmic_size     = NRF_MESH_TRANSMIC_SIZE_DEFAULT;
    reliable.message.access_token      = nrf_mesh_unique_token_get();
    reliable.reply_opcode.opcode       = reply_opcode;
    reliable.reply_opcode.company_id   = COMMAND_COMPANY_ID;
    reliable.timeout                   = COMMAND_CLIENT_ACKED_TRANSACTION_TIMEOUT;
    reliable.status_cb                 = reliable_status_cb;

    return access_model_reliable_publish(&reliable);
}

/*****************************************************************************
 * Opcode handler callback(s)
 *****************************************************************************/
// node注册回调
static void handle_register_cb(access_model_handle_t handle, const access_message_rx_t* p_message, void* p_args)
{
    command_client_t* p_client = p_args;
    NRF_MESH_ASSERT(p_client->register_cb != NULL);

    command_msg_register_t reg = *(command_msg_register_t*)p_message->p_data;
    p_client->register_cb(p_client, reg.type, p_message->meta_data.src.value);
}

// 设置状态回调
static void handle_status_cb(access_model_handle_t handle, const access_message_rx_t* p_message, void* p_args)
{
    command_client_t* p_client = p_args;
    NRF_MESH_ASSERT(p_client->status_cb != NULL);

    command_msg_status_t status = *(command_msg_status_t*)p_message->p_data;
    p_client->status_cb(p_client, status.data, p_message->meta_data.src.value);
}

// 读取数据回调
static void handle_data_cb(access_model_handle_t handle, const access_message_rx_t* p_message, void* p_args)
{
    command_client_t* p_client = p_args;
    NRF_MESH_ASSERT(p_client->data_cb != NULL);

    command_msg_data_t data = *(command_msg_data_t*)p_message->p_data;
    p_client->data_cb(p_client, data.data, p_message->meta_data.src.value);
}

static const access_opcode_handler_t m_opcode_handlers[] = {{ACCESS_OPCODE_VENDOR(COMMAND_OPCODE_REGISTER, COMMAND_COMPANY_ID), handle_register_cb},
                                                            {ACCESS_OPCODE_VENDOR(COMMAND_OPCODE_STATUS, COMMAND_COMPANY_ID), handle_status_cb},
                                                            {ACCESS_OPCODE_VENDOR(COMMAND_OPCODE_DATA, COMMAND_COMPANY_ID), handle_data_cb}};

static void handle_publish_timeout(access_model_handle_t handle, void* p_args)
{
    command_client_t* p_client = p_args;

    if (p_client->timeout_cb != NULL) {
        p_client->timeout_cb(handle, p_args);
    }
}
/*****************************************************************************
 * Public API
 *****************************************************************************/

uint32_t command_client_init(command_client_t* p_client, uint16_t element_index)
{
    if (p_client == NULL || p_client->status_cb == NULL) {
        return NRF_ERROR_NULL;
    }

    access_model_add_params_t init_params;
    init_params.model_id.model_id   = COMMAND_CLIENT_MODEL_ID;
    init_params.model_id.company_id = COMMAND_COMPANY_ID;
    init_params.element_index       = element_index;
    init_params.p_opcode_handlers   = &m_opcode_handlers[0];
    init_params.opcode_count        = sizeof(m_opcode_handlers) / sizeof(m_opcode_handlers[0]);
    init_params.p_args              = p_client;
    init_params.publish_timeout_cb  = handle_publish_timeout;
    return access_model_add(&init_params, &p_client->model_handle);
}

uint32_t command_client_find(command_client_t* p_client)
{
    if (p_client == NULL || p_client->register_cb == NULL) {
        return NRF_ERROR_NULL;
    }
    // else if (p_client->state.reliable_transfer_active) {
    //     return NRF_ERROR_INVALID_STATE;
    // }

    RETURN_ON_ERROR(dsm_address_publish_add(COMMAND_GROUP_ADDRESS, &m_publish_address_handle));
    RETURN_ON_ERROR(access_model_publish_address_set(p_client->model_handle, m_publish_address_handle));

    uint32_t status = send_reliable_message(p_client, COMMAND_OPCODE_FIND, NULL, 0);
    // if (status == NRF_SUCCESS) {
    //     p_client->state.reliable_transfer_active = true;
    // }
    return status;
}

uint32_t command_client_get(command_client_t* p_client, uint16_t publish_address)
{
    if (p_client == NULL || p_client->data_cb == NULL) {
        return NRF_ERROR_NULL;
    }
    // else if (p_client->state.reliable_transfer_active) {
    //     return NRF_ERROR_INVALID_STATE;
    // }

    RETURN_ON_ERROR(dsm_address_publish_add(publish_address, &m_publish_address_handle));
    RETURN_ON_ERROR(access_model_publish_address_set(p_client->model_handle, m_publish_address_handle));

    uint32_t status = send_reliable_message(p_client, COMMAND_OPCODE_GET, NULL, 0);
    // if (status == NRF_SUCCESS) {
    //     p_client->state.reliable_transfer_active = true;
    // }
    return status;
}

uint32_t command_client_set(command_client_t* p_client, uint16_t publish_address, command_msg_set_t set_msg)
{
    if (p_client == NULL || p_client->status_cb == NULL) {
        return NRF_ERROR_NULL;
    }
    // else if (p_client->state.reliable_transfer_active) {
    //     return NRF_ERROR_INVALID_STATE;
    // }

    RETURN_ON_ERROR(dsm_address_publish_add(publish_address, &m_publish_address_handle));
    RETURN_ON_ERROR(access_model_publish_address_set(p_client->model_handle, m_publish_address_handle));

    set_msg.tid     = m_tid++;
    uint32_t status = send_reliable_message(p_client, COMMAND_OPCODE_SET, (const uint8_t*)&set_msg, sizeof(set_msg));
    // if (status == NRF_SUCCESS) {
    //     p_client->state.reliable_transfer_active = true;
    // }
    return status;
}

/**
 * Cancel any ongoing reliable message transfer.
 *
 * @param[in] p_client Pointer to the client instance structure.
 */
void command_client_pending_msg_cancel(command_client_t* p_client)
{
    (void)access_model_reliable_cancel(p_client->model_handle);
}
