#include "command_server.h"
#include "command_common.h"

#include <stddef.h>
#include <stdint.h>

#include "access.h"
#include "log.h"
#include "nrf_mesh_assert.h"

/*****************************************************************************
 * Static functions
 *****************************************************************************/

static void reply_status(const command_server_t* p_server, const access_message_rx_t* p_message, uint16_t opcode, const uint8_t* buffer, uint16_t length)
{
    access_message_tx_t reply;
    reply.opcode.opcode     = opcode;
    reply.opcode.company_id = COMMAND_COMPANY_ID;
    reply.p_buffer          = buffer;
    reply.length            = length;
    reply.force_segmented   = false;
    reply.transmic_size     = NRF_MESH_TRANSMIC_SIZE_DEFAULT;
    reply.access_token      = nrf_mesh_unique_token_get();

    (void)access_model_reply(p_server->model_handle, p_message, &reply);
}

/*****************************************************************************
 * Opcode handler callbacks
 *****************************************************************************/

static void handle_set_cb(access_model_handle_t handle, const access_message_rx_t* p_message, void* p_args)
{
    command_server_t* p_server = p_args;
    NRF_MESH_ASSERT(p_server->set_cb != NULL);

    command_msg_set_t    set_msg    = *(command_msg_set_t*)p_message->p_data;
    command_msg_status_t status_msg = p_server->set_cb(p_server, set_msg);
    reply_status(p_server, p_message, COMMAND_OPCODE_STATUS, (const uint8_t*)&status_msg, sizeof(status_msg));
}

static void handle_get_cb(access_model_handle_t handle, const access_message_rx_t* p_message, void* p_args)
{
    command_server_t* p_server = p_args;
    NRF_MESH_ASSERT(p_server->get_cb != NULL);
    command_msg_data_t data_msg = p_server->get_cb(p_server);
    reply_status(p_server, p_message, COMMAND_OPCODE_DATA, (const uint8_t*)&data_msg, sizeof(data_msg));
}

static void handle_find_cb(access_model_handle_t handle, const access_message_rx_t* p_message, void* p_args)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Receive find command  \n");

    command_server_t*      p_server     = p_args;
    command_msg_register_t register_msg = {.type = p_server->server_type};
    reply_status(p_server, p_message, COMMAND_OPCODE_REGISTER, (const uint8_t*)&register_msg, sizeof(register_msg));
}

static const access_opcode_handler_t m_opcode_handlers[] = {{ACCESS_OPCODE_VENDOR(COMMAND_OPCODE_FIND, COMMAND_COMPANY_ID), handle_find_cb},
                                                            {ACCESS_OPCODE_VENDOR(COMMAND_OPCODE_SET, COMMAND_COMPANY_ID), handle_set_cb},
                                                            {ACCESS_OPCODE_VENDOR(COMMAND_OPCODE_GET, COMMAND_COMPANY_ID), handle_get_cb}

};

static void handle_publish_timeout(access_model_handle_t handle, void* p_args)
{
    (void)handle;
    (void)p_args;
    // UNUSED_PARAMETER
    // command_server_t* p_server = p_args;

    // (void)command_server_status_publish(p_server, p_server->get_cb(p_server));
}

/*****************************************************************************
 * Public API
 *****************************************************************************/

uint32_t command_server_init(command_server_t* p_server, uint16_t element_index, command_device_t server_type)
{
    if (p_server == NULL || p_server->get_cb == NULL || p_server->set_cb == NULL) {
        return NRF_ERROR_NULL;
    }

    p_server->server_type = server_type;

    access_model_add_params_t init_params;
    init_params.element_index       = element_index;
    init_params.model_id.model_id   = COMMAND_SERVER_MODEL_ID;
    init_params.model_id.company_id = COMMAND_COMPANY_ID;
    init_params.p_opcode_handlers   = &m_opcode_handlers[0];
    init_params.opcode_count        = sizeof(m_opcode_handlers) / sizeof(m_opcode_handlers[0]);
    init_params.p_args              = p_server;
    init_params.publish_timeout_cb  = handle_publish_timeout;
    return access_model_add(&init_params, &p_server->model_handle);
}
