#ifndef COMMAND_SERVER_H__
#define COMMAND_SERVER_H__

#include "access.h"
#include "command_common.h"
#include <stdbool.h>
#include <stdint.h>

/**
 * @defgroup COMMAND_SERVER Simple OnOff Server
 * @ingroup COMMAND_MODEL
 * This module implements a vendor specific Simple OnOff Server.
 * @{
 */

/** model ID. */
#define COMMAND_SERVER_MODEL_ID (0x0000)

/** Forward declaration. */
typedef struct __command_server command_server_t;

typedef command_msg_data_t (*command_get_cb_t)(const command_server_t* p_self);
typedef command_msg_status_t (*command_set_cb_t)(const command_server_t* p_self, command_msg_set_t set_msg);

/** Simple OnOff Server state structure. */
struct __command_server
{
    /** Model handle assigned to the server. */
    access_model_handle_t model_handle;
    command_get_cb_t      get_cb;
    command_set_cb_t      set_cb;
    command_device_t      server_type;
};

// 初始化model
uint32_t command_server_init(command_server_t* p_server, uint16_t element_index, command_device_t server_type);

/** @} end of COMMAND_SERVER */

#endif /* COMMAND_SERVER_H__ */
