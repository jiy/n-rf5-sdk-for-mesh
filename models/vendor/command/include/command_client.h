#ifndef COMMAND_CLIENT_H__
#define COMMAND_CLIENT_H__

#include "access.h"
#include "command_common.h"
#include <stdint.h>

/**
 * @defgroup COMMAND_CLIENT Simple OnOff Client
 * @ingroup COMMAND_MODEL
 * This module implements a vendor specific Simple OnOff Client.
 *
 * @{
 */

/** Acknowledged message transaction timeout */
#ifndef COMMAND_CLIENT_ACKED_TRANSACTION_TIMEOUT
#    define COMMAND_CLIENT_ACKED_TRANSACTION_TIMEOUT (SEC_TO_US(6))
#endif

/** Simple OnOff Client model ID. */
#define COMMAND_CLIENT_MODEL_ID (0x0001)

/** Forward declaration. */
typedef struct __command_client command_client_t;

typedef void (*command_register_cb_t)(const command_client_t* p_self, uint8_t type, uint16_t src);
typedef void (*command_status_cb_t)(const command_client_t* p_self, uint8_t* status, uint16_t src);
typedef void (*command_data_cb_t)(const command_client_t* p_self, uint8_t* data, uint16_t src);

/**
 * Simple OnOff timeout callback type.
 *
 * @param[in] handle Model handle
 * @param[in] p_self Pointer to the Simple OnOff client structure that received the status.
 */
typedef void (*command_timeout_cb_t)(access_model_handle_t handle, void* p_self);

/** Simple OnOff Client state structure. */
struct __command_client
{
    /** Model handle assigned to the client. */
    access_model_handle_t model_handle;
    /** Status callback called after status received from server. */
    command_register_cb_t register_cb;
    command_status_cb_t   status_cb;
    command_data_cb_t     data_cb;
    /** Periodic timer timeout callback used for periodic publication. */
    command_timeout_cb_t timeout_cb;
    /** Internal client state. */
    struct
    {
        bool              reliable_transfer_active; /**< Variable used to determine if a transfer is currently active. */
        command_msg_set_t data;                     /**< Variable reflecting the data stored in the server. */
    } state;
};

uint32_t command_client_init(command_client_t* p_client, uint16_t element_index);  // 初始化model
uint32_t command_client_find(command_client_t* p_client);                          // 发现设备
uint32_t command_client_get(command_client_t* p_client, uint16_t publish_address);
uint32_t command_client_set(command_client_t* p_client, uint16_t publish_address, command_msg_set_t set_msg);

/**
 * Cancel any ongoing reliable message transfer.
 *
 * @param[in] p_client Pointer to the client instance structure.
 */
void command_client_pending_msg_cancel(command_client_t* p_client);

/** @} end of COMMAND_CLIENT */

#endif /* COMMAND_CLIENT_H__ */
