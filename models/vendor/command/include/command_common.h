#ifndef COMMAND_COMMON_H__
#define COMMAND_COMMON_H__

#include "access.h"
#include <stdint.h>

/** Vendor specific company ID for Simple OnOff model */
#define COMMAND_COMPANY_ID (ACCESS_COMPANY_ID_NORDIC)

#ifndef COMMAND_GROUP_ADDRESS  // 默认组播地址
#    define COMMAND_GROUP_ADDRESS 0xC023
#endif

#ifndef COMMAND_DATA_LENGTH  // set和data option的数据长度，默认为8
#    define COMMAND_DATA_LENGTH 8
#endif

typedef enum {
    COMMAND_SERVER_LIGHT   = 1,  // 灯
    COMMAND_SERVER_SWITCH  = 2,  // 开关
    COMMAND_SERVER_COUNTER = 3   // 计数器
} command_device_t;

/** Simple OnOff opcodes. */
typedef enum {
    COMMAND_OPCODE_FIND     = 0xC1,  // 网关寻找设备
    COMMAND_OPCODE_REGISTER = 0xC2,  // 设备向网关提交注册
    COMMAND_OPCODE_SET      = 0xC3,  // 设置设备
    COMMAND_OPCODE_STATUS   = 0xC4,  // 设备状态
    COMMAND_OPCODE_GET      = 0xC5,  // 读取设备
    COMMAND_OPCODE_DATA     = 0xC6   // 设备数据
} command_opcode_t;

typedef struct __attribute((packed))
{
    uint8_t type;
} command_msg_register_t;

typedef struct __attribute((packed))
{
    uint8_t data[COMMAND_DATA_LENGTH];
} command_msg_data_t;

typedef struct __attribute((packed))
{
    uint8_t data[COMMAND_DATA_LENGTH];
    uint8_t tid;
} command_msg_set_t;

typedef struct __attribute((packed))
{
    uint8_t data[COMMAND_DATA_LENGTH];
} command_msg_status_t;

/*lint -align_max(pop) */

/** @} end of COMMAND_COMMON */
/** @} end of COMMAND_MODEL */
#endif /* COMMAND_COMMON_H__ */
